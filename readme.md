Instructions
============



How to start a new project for your own code?
---------------------------------------------

* copy 'template.X', 'plib' and 'drivers' folders to a new folder 
* open the created folder in mplab X
* right click on the project to rename it as you wish
    
