Pour observer 6 variables du programme
--------------------------------------

1. Dans le projet template.X, il faut assigner les valeurs dans 
	long scopeVariables[6]
Les 4 premi�res sont sous forme de graphiques en temps r�el, les 2 derni�res sont affich�es sous forme num�rique.
2. programmer la carte
3. Changer le nom des variables dans matlab/name.txt
4. changer lՎtendue des variables � observer dans matlab/ranges.txt
5. lancer le script scope.m
    * ouvir matlab 
    * changer le dossier courant (dans la bar au dessus) pour pointer le dossier llsmf2018/matlab
    * dans la fen�tre de commande, taper �scope�


Attention
---------
- les variables ne sont mises � jour que l� o� vous le faites dans le programme 
- les variables ne sont envoy�es que 10 fois par seconde (les �v�nements tr�s brefs peuvent ne pas apparaitre)
- les variables � observer doivent �tre de type entier (char short ou long) sign� ou non-sign�.

Exemple
-------

Je veux afficher :
- les acc�l�rations sur trois axes (ax, ay et az) sous la forme d'un graphique en temps r�el
- afficher le temps �coul� en seconde et la temp�rature sous forme num�rique.


1. Dans le projet template.X, dans le while(1) (o� � un autre endroit o� on passe au moins 10 fois par seconde):
    scopeVariables[0] = ax; // acc�l�ration selon l'axe x
    scopeVariables[1] = ay; // acc�l�ration selon l'axe y
    scopeVariables[2] = az; // acc�l�ration selon l'axe z
    // scopeVariables[3] pas utilis�
    scopeVariables[4] = interruptCount  // interruptCount est incr�ment� une fois par seconde
    scopeVariables[5] = temperature     // temp�rature en degr�
    
2. programmer la carte
3. dans le fichier names.txt, �crire:
    ax,ay,az,,time,temperature
4. dans le fichier ranges.txt, �crire:
min,max
-60,60
-60,60
-60,60
0,1

En supposant que les acc�l�rations soient toujours comprises entre -60 et 60 [(m/s^2)/10]

5. lancer le script scope.m
    * ouvir matlab 
    * changer le dossier courant (dans la bar au dessus) pour pointer le dossier llsmf2018/matlab
    * dans la fen�tre de commandes, taper �scope�


