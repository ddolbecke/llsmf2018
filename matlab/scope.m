clear all
close all

ranges=csvread('ranges.txt',1,0);
names=textread('names.txt','%s','delimiter',',');

frameSize=5;   %[s]
acqTime=200;    %[s]
fs=10;         %sampling frequency

s=serial('/dev/tty.usbserial-AJV9IK97','BaudRate',57600);
set(s, 'DataBits', 8);
set(s, 'StopBits', 1);
set(s, 'Parity', 'none');


fopen(s);

try
    x=zeros(frameSize*fs,6);
    t=linspace(0,frameSize,frameSize*fs);
    i=frameSize*fs;

    a = 0;
    v=1;
    
    subplot(5,1,v)
    p1=plot(t,x(:,1),'.-');
    title(names(v));
    ylim(ranges(v,:));
    xlim([0,frameSize]);
    v=v+1;

    subplot(5,1,v)
    p2=plot(t,x(:,1),'.-');
    title(names(v));
    ylim(ranges(v,:));
    xlim([0,frameSize]);
    v=v+1;

    subplot(5,1,v)
    p3=plot(t,x(:,1),'.-');
    title(names(v));
    ylim(ranges(v,:));
    xlim([0,frameSize]);
    v=v+1;


    subplot(5,1,v)
    p4=plot(t,x(:,1),'.-');
    title(names(v));
    ylim(ranges(v,:));
    xlim([0,frameSize]);
    v=v+1;

    subplot(5,1,5)
    text(0.25,0.8,names(5),'FontSize',14,'FontWeight','bold');
    text(0.75,0.8,names(6),'FontSize',14,'FontWeight','bold');
    t1=text(0.25,0.4,'','FontSize',14,'FontWeight','bold');
    t2=text(0.75,0.4,'','FontSize',14,'FontWeight','bold');

    while (i<acqTime*fs)


        a = fscanf(s,'%s');
        aa=sscanf(a,'%d,%d,%d,%d,%d,%d');
        if(length(aa) ==6)    

            x(i,1:6) = aa;


            set(p1,'YData',x(i-frameSize*fs+1:i,1))
            set(p2,'YData',x(i-frameSize*fs+1:i,2))
            set(p3,'YData',x(i-frameSize*fs+1:i,3))
            set(p4,'YData',x(i-frameSize*fs+1:i,4))

            set(t1,'str',aa(1))
            set(t2,'str',aa(6))


            i=i+1;
            drawnow;
        end
    end
end

fclose(s)
disp('end')

