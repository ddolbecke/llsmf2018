#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/template.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/template.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=sources/main.c ../drivers/ChrFont0.c ../drivers/ClpLib.c ../drivers/delay.c ../drivers/FillPat.c ../drivers/PmodOLED.c ../drivers/motors.c ../drivers/servo.c ../drivers/interrupts.c ../drivers/acl.c ../drivers/PmodCLP.c ../drivers/PmodBTN.c ../drivers/mx4.c ../drivers/Pmod8leds.c ../drivers/PmodSSD.c ../drivers/OledChar.c ../drivers/OledGrph.c ../drivers/PmodTMP3.c ../drivers/PmodMAXSONAR.c ../drivers/scope.c ../drivers/analogInputs.c ../drivers/databk.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/sources/main.o ${OBJECTDIR}/_ext/239857660/ChrFont0.o ${OBJECTDIR}/_ext/239857660/ClpLib.o ${OBJECTDIR}/_ext/239857660/delay.o ${OBJECTDIR}/_ext/239857660/FillPat.o ${OBJECTDIR}/_ext/239857660/PmodOLED.o ${OBJECTDIR}/_ext/239857660/motors.o ${OBJECTDIR}/_ext/239857660/servo.o ${OBJECTDIR}/_ext/239857660/interrupts.o ${OBJECTDIR}/_ext/239857660/acl.o ${OBJECTDIR}/_ext/239857660/PmodCLP.o ${OBJECTDIR}/_ext/239857660/PmodBTN.o ${OBJECTDIR}/_ext/239857660/mx4.o ${OBJECTDIR}/_ext/239857660/Pmod8leds.o ${OBJECTDIR}/_ext/239857660/PmodSSD.o ${OBJECTDIR}/_ext/239857660/OledChar.o ${OBJECTDIR}/_ext/239857660/OledGrph.o ${OBJECTDIR}/_ext/239857660/PmodTMP3.o ${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o ${OBJECTDIR}/_ext/239857660/scope.o ${OBJECTDIR}/_ext/239857660/analogInputs.o ${OBJECTDIR}/_ext/239857660/databk.o
POSSIBLE_DEPFILES=${OBJECTDIR}/sources/main.o.d ${OBJECTDIR}/_ext/239857660/ChrFont0.o.d ${OBJECTDIR}/_ext/239857660/ClpLib.o.d ${OBJECTDIR}/_ext/239857660/delay.o.d ${OBJECTDIR}/_ext/239857660/FillPat.o.d ${OBJECTDIR}/_ext/239857660/PmodOLED.o.d ${OBJECTDIR}/_ext/239857660/motors.o.d ${OBJECTDIR}/_ext/239857660/servo.o.d ${OBJECTDIR}/_ext/239857660/interrupts.o.d ${OBJECTDIR}/_ext/239857660/acl.o.d ${OBJECTDIR}/_ext/239857660/PmodCLP.o.d ${OBJECTDIR}/_ext/239857660/PmodBTN.o.d ${OBJECTDIR}/_ext/239857660/mx4.o.d ${OBJECTDIR}/_ext/239857660/Pmod8leds.o.d ${OBJECTDIR}/_ext/239857660/PmodSSD.o.d ${OBJECTDIR}/_ext/239857660/OledChar.o.d ${OBJECTDIR}/_ext/239857660/OledGrph.o.d ${OBJECTDIR}/_ext/239857660/PmodTMP3.o.d ${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o.d ${OBJECTDIR}/_ext/239857660/scope.o.d ${OBJECTDIR}/_ext/239857660/analogInputs.o.d ${OBJECTDIR}/_ext/239857660/databk.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/sources/main.o ${OBJECTDIR}/_ext/239857660/ChrFont0.o ${OBJECTDIR}/_ext/239857660/ClpLib.o ${OBJECTDIR}/_ext/239857660/delay.o ${OBJECTDIR}/_ext/239857660/FillPat.o ${OBJECTDIR}/_ext/239857660/PmodOLED.o ${OBJECTDIR}/_ext/239857660/motors.o ${OBJECTDIR}/_ext/239857660/servo.o ${OBJECTDIR}/_ext/239857660/interrupts.o ${OBJECTDIR}/_ext/239857660/acl.o ${OBJECTDIR}/_ext/239857660/PmodCLP.o ${OBJECTDIR}/_ext/239857660/PmodBTN.o ${OBJECTDIR}/_ext/239857660/mx4.o ${OBJECTDIR}/_ext/239857660/Pmod8leds.o ${OBJECTDIR}/_ext/239857660/PmodSSD.o ${OBJECTDIR}/_ext/239857660/OledChar.o ${OBJECTDIR}/_ext/239857660/OledGrph.o ${OBJECTDIR}/_ext/239857660/PmodTMP3.o ${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o ${OBJECTDIR}/_ext/239857660/scope.o ${OBJECTDIR}/_ext/239857660/analogInputs.o ${OBJECTDIR}/_ext/239857660/databk.o

# Source Files
SOURCEFILES=sources/main.c ../drivers/ChrFont0.c ../drivers/ClpLib.c ../drivers/delay.c ../drivers/FillPat.c ../drivers/PmodOLED.c ../drivers/motors.c ../drivers/servo.c ../drivers/interrupts.c ../drivers/acl.c ../drivers/PmodCLP.c ../drivers/PmodBTN.c ../drivers/mx4.c ../drivers/Pmod8leds.c ../drivers/PmodSSD.c ../drivers/OledChar.c ../drivers/OledGrph.c ../drivers/PmodTMP3.c ../drivers/PmodMAXSONAR.c ../drivers/scope.c ../drivers/analogInputs.c ../drivers/databk.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/template.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX460F512L
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/sources/main.o: sources/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sources" 
	@${RM} ${OBJECTDIR}/sources/main.o.d 
	@${RM} ${OBJECTDIR}/sources/main.o 
	@${FIXDEPS} "${OBJECTDIR}/sources/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/sources/main.o.d" -o ${OBJECTDIR}/sources/main.o sources/main.c   
	
${OBJECTDIR}/_ext/239857660/ChrFont0.o: ../drivers/ChrFont0.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/ChrFont0.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/ChrFont0.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/ChrFont0.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/ChrFont0.o.d" -o ${OBJECTDIR}/_ext/239857660/ChrFont0.o ../drivers/ChrFont0.c   
	
${OBJECTDIR}/_ext/239857660/ClpLib.o: ../drivers/ClpLib.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/ClpLib.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/ClpLib.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/ClpLib.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/ClpLib.o.d" -o ${OBJECTDIR}/_ext/239857660/ClpLib.o ../drivers/ClpLib.c   
	
${OBJECTDIR}/_ext/239857660/delay.o: ../drivers/delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/delay.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/delay.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/delay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/delay.o.d" -o ${OBJECTDIR}/_ext/239857660/delay.o ../drivers/delay.c   
	
${OBJECTDIR}/_ext/239857660/FillPat.o: ../drivers/FillPat.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/FillPat.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/FillPat.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/FillPat.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/FillPat.o.d" -o ${OBJECTDIR}/_ext/239857660/FillPat.o ../drivers/FillPat.c   
	
${OBJECTDIR}/_ext/239857660/PmodOLED.o: ../drivers/PmodOLED.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodOLED.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodOLED.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/PmodOLED.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/PmodOLED.o.d" -o ${OBJECTDIR}/_ext/239857660/PmodOLED.o ../drivers/PmodOLED.c   
	
${OBJECTDIR}/_ext/239857660/motors.o: ../drivers/motors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/motors.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/motors.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/motors.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/motors.o.d" -o ${OBJECTDIR}/_ext/239857660/motors.o ../drivers/motors.c   
	
${OBJECTDIR}/_ext/239857660/servo.o: ../drivers/servo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/servo.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/servo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/servo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/servo.o.d" -o ${OBJECTDIR}/_ext/239857660/servo.o ../drivers/servo.c   
	
${OBJECTDIR}/_ext/239857660/interrupts.o: ../drivers/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/interrupts.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/interrupts.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/interrupts.o.d" -o ${OBJECTDIR}/_ext/239857660/interrupts.o ../drivers/interrupts.c   
	
${OBJECTDIR}/_ext/239857660/acl.o: ../drivers/acl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/acl.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/acl.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/acl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/acl.o.d" -o ${OBJECTDIR}/_ext/239857660/acl.o ../drivers/acl.c   
	
${OBJECTDIR}/_ext/239857660/PmodCLP.o: ../drivers/PmodCLP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodCLP.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodCLP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/PmodCLP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/PmodCLP.o.d" -o ${OBJECTDIR}/_ext/239857660/PmodCLP.o ../drivers/PmodCLP.c   
	
${OBJECTDIR}/_ext/239857660/PmodBTN.o: ../drivers/PmodBTN.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodBTN.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodBTN.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/PmodBTN.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/PmodBTN.o.d" -o ${OBJECTDIR}/_ext/239857660/PmodBTN.o ../drivers/PmodBTN.c   
	
${OBJECTDIR}/_ext/239857660/mx4.o: ../drivers/mx4.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/mx4.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/mx4.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/mx4.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/mx4.o.d" -o ${OBJECTDIR}/_ext/239857660/mx4.o ../drivers/mx4.c   
	
${OBJECTDIR}/_ext/239857660/Pmod8leds.o: ../drivers/Pmod8leds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/Pmod8leds.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/Pmod8leds.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/Pmod8leds.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/Pmod8leds.o.d" -o ${OBJECTDIR}/_ext/239857660/Pmod8leds.o ../drivers/Pmod8leds.c   
	
${OBJECTDIR}/_ext/239857660/PmodSSD.o: ../drivers/PmodSSD.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodSSD.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodSSD.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/PmodSSD.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/PmodSSD.o.d" -o ${OBJECTDIR}/_ext/239857660/PmodSSD.o ../drivers/PmodSSD.c   
	
${OBJECTDIR}/_ext/239857660/OledChar.o: ../drivers/OledChar.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/OledChar.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/OledChar.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/OledChar.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/OledChar.o.d" -o ${OBJECTDIR}/_ext/239857660/OledChar.o ../drivers/OledChar.c   
	
${OBJECTDIR}/_ext/239857660/OledGrph.o: ../drivers/OledGrph.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/OledGrph.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/OledGrph.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/OledGrph.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/OledGrph.o.d" -o ${OBJECTDIR}/_ext/239857660/OledGrph.o ../drivers/OledGrph.c   
	
${OBJECTDIR}/_ext/239857660/PmodTMP3.o: ../drivers/PmodTMP3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodTMP3.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodTMP3.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/PmodTMP3.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/PmodTMP3.o.d" -o ${OBJECTDIR}/_ext/239857660/PmodTMP3.o ../drivers/PmodTMP3.c   
	
${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o: ../drivers/PmodMAXSONAR.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o.d" -o ${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o ../drivers/PmodMAXSONAR.c   
	
${OBJECTDIR}/_ext/239857660/scope.o: ../drivers/scope.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/scope.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/scope.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/scope.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/scope.o.d" -o ${OBJECTDIR}/_ext/239857660/scope.o ../drivers/scope.c   
	
${OBJECTDIR}/_ext/239857660/analogInputs.o: ../drivers/analogInputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/analogInputs.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/analogInputs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/analogInputs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/analogInputs.o.d" -o ${OBJECTDIR}/_ext/239857660/analogInputs.o ../drivers/analogInputs.c   
	
${OBJECTDIR}/_ext/239857660/databk.o: ../drivers/databk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/databk.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/databk.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/databk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/databk.o.d" -o ${OBJECTDIR}/_ext/239857660/databk.o ../drivers/databk.c   
	
else
${OBJECTDIR}/sources/main.o: sources/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/sources" 
	@${RM} ${OBJECTDIR}/sources/main.o.d 
	@${RM} ${OBJECTDIR}/sources/main.o 
	@${FIXDEPS} "${OBJECTDIR}/sources/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/sources/main.o.d" -o ${OBJECTDIR}/sources/main.o sources/main.c   
	
${OBJECTDIR}/_ext/239857660/ChrFont0.o: ../drivers/ChrFont0.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/ChrFont0.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/ChrFont0.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/ChrFont0.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/ChrFont0.o.d" -o ${OBJECTDIR}/_ext/239857660/ChrFont0.o ../drivers/ChrFont0.c   
	
${OBJECTDIR}/_ext/239857660/ClpLib.o: ../drivers/ClpLib.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/ClpLib.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/ClpLib.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/ClpLib.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/ClpLib.o.d" -o ${OBJECTDIR}/_ext/239857660/ClpLib.o ../drivers/ClpLib.c   
	
${OBJECTDIR}/_ext/239857660/delay.o: ../drivers/delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/delay.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/delay.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/delay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/delay.o.d" -o ${OBJECTDIR}/_ext/239857660/delay.o ../drivers/delay.c   
	
${OBJECTDIR}/_ext/239857660/FillPat.o: ../drivers/FillPat.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/FillPat.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/FillPat.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/FillPat.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/FillPat.o.d" -o ${OBJECTDIR}/_ext/239857660/FillPat.o ../drivers/FillPat.c   
	
${OBJECTDIR}/_ext/239857660/PmodOLED.o: ../drivers/PmodOLED.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodOLED.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodOLED.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/PmodOLED.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/PmodOLED.o.d" -o ${OBJECTDIR}/_ext/239857660/PmodOLED.o ../drivers/PmodOLED.c   
	
${OBJECTDIR}/_ext/239857660/motors.o: ../drivers/motors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/motors.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/motors.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/motors.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/motors.o.d" -o ${OBJECTDIR}/_ext/239857660/motors.o ../drivers/motors.c   
	
${OBJECTDIR}/_ext/239857660/servo.o: ../drivers/servo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/servo.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/servo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/servo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/servo.o.d" -o ${OBJECTDIR}/_ext/239857660/servo.o ../drivers/servo.c   
	
${OBJECTDIR}/_ext/239857660/interrupts.o: ../drivers/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/interrupts.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/interrupts.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/interrupts.o.d" -o ${OBJECTDIR}/_ext/239857660/interrupts.o ../drivers/interrupts.c   
	
${OBJECTDIR}/_ext/239857660/acl.o: ../drivers/acl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/acl.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/acl.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/acl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/acl.o.d" -o ${OBJECTDIR}/_ext/239857660/acl.o ../drivers/acl.c   
	
${OBJECTDIR}/_ext/239857660/PmodCLP.o: ../drivers/PmodCLP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodCLP.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodCLP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/PmodCLP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/PmodCLP.o.d" -o ${OBJECTDIR}/_ext/239857660/PmodCLP.o ../drivers/PmodCLP.c   
	
${OBJECTDIR}/_ext/239857660/PmodBTN.o: ../drivers/PmodBTN.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodBTN.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodBTN.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/PmodBTN.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/PmodBTN.o.d" -o ${OBJECTDIR}/_ext/239857660/PmodBTN.o ../drivers/PmodBTN.c   
	
${OBJECTDIR}/_ext/239857660/mx4.o: ../drivers/mx4.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/mx4.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/mx4.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/mx4.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/mx4.o.d" -o ${OBJECTDIR}/_ext/239857660/mx4.o ../drivers/mx4.c   
	
${OBJECTDIR}/_ext/239857660/Pmod8leds.o: ../drivers/Pmod8leds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/Pmod8leds.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/Pmod8leds.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/Pmod8leds.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/Pmod8leds.o.d" -o ${OBJECTDIR}/_ext/239857660/Pmod8leds.o ../drivers/Pmod8leds.c   
	
${OBJECTDIR}/_ext/239857660/PmodSSD.o: ../drivers/PmodSSD.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodSSD.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodSSD.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/PmodSSD.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/PmodSSD.o.d" -o ${OBJECTDIR}/_ext/239857660/PmodSSD.o ../drivers/PmodSSD.c   
	
${OBJECTDIR}/_ext/239857660/OledChar.o: ../drivers/OledChar.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/OledChar.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/OledChar.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/OledChar.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/OledChar.o.d" -o ${OBJECTDIR}/_ext/239857660/OledChar.o ../drivers/OledChar.c   
	
${OBJECTDIR}/_ext/239857660/OledGrph.o: ../drivers/OledGrph.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/OledGrph.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/OledGrph.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/OledGrph.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/OledGrph.o.d" -o ${OBJECTDIR}/_ext/239857660/OledGrph.o ../drivers/OledGrph.c   
	
${OBJECTDIR}/_ext/239857660/PmodTMP3.o: ../drivers/PmodTMP3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodTMP3.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodTMP3.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/PmodTMP3.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/PmodTMP3.o.d" -o ${OBJECTDIR}/_ext/239857660/PmodTMP3.o ../drivers/PmodTMP3.c   
	
${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o: ../drivers/PmodMAXSONAR.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o.d" -o ${OBJECTDIR}/_ext/239857660/PmodMAXSONAR.o ../drivers/PmodMAXSONAR.c   
	
${OBJECTDIR}/_ext/239857660/scope.o: ../drivers/scope.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/scope.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/scope.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/scope.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/scope.o.d" -o ${OBJECTDIR}/_ext/239857660/scope.o ../drivers/scope.c   
	
${OBJECTDIR}/_ext/239857660/analogInputs.o: ../drivers/analogInputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/analogInputs.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/analogInputs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/analogInputs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/analogInputs.o.d" -o ${OBJECTDIR}/_ext/239857660/analogInputs.o ../drivers/analogInputs.c   
	
${OBJECTDIR}/_ext/239857660/databk.o: ../drivers/databk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/239857660" 
	@${RM} ${OBJECTDIR}/_ext/239857660/databk.o.d 
	@${RM} ${OBJECTDIR}/_ext/239857660/databk.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/239857660/databk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"sources" -I"../plib/pic32mx/include" -I"../drivers" -MMD -MF "${OBJECTDIR}/_ext/239857660/databk.o.d" -o ${OBJECTDIR}/_ext/239857660/databk.o ../drivers/databk.c   
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/template.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../plib/pic32mx/lib/libmchp_peripheral_32MX460F512L.a ../plib/pic32mx/lib/libmchp_peripheral.a  
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_PK3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/template.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ../plib/pic32mx/lib/libmchp_peripheral_32MX460F512L.a ../plib/pic32mx/lib/libmchp_peripheral.a       -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC02000:0x1FC02FEF -mreserve=boot@0x1FC02000:0x1FC024FF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/template.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../plib/pic32mx/lib/libmchp_peripheral_32MX460F512L.a ../plib/pic32mx/lib/libmchp_peripheral.a 
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/template.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ../plib/pic32mx/lib/libmchp_peripheral_32MX460F512L.a ../plib/pic32mx/lib/libmchp_peripheral.a      -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	${MP_CC_DIR}/xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/template.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
