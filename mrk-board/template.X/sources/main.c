/* 
 * File:   main.c
 * Author: Dimitri
 *
 * Created on October 6, 2015, 1:54 PM
 */


/* ------------------------------------------------------------ */
/*				main header     								*/
/* ------------------------------------------------------------ */
#include "main.h"
/* ------------------------------------------------------------ */
/*				peripheral modules								*/
/* ------------------------------------------------------------ */
#include "delay.h"
#include "PmodMAXSONAR.h"
#include "scope.h"
#include "databk.h"
/* ------------------------------------------------------------ */
/*				global variables								*/
/* ------------------------------------------------------------ */
unsigned short databkvar[3];
/* ------------------------------------------------------------ */
/*				main function     								*/
/* ------------------------------------------------------------ */
void main(void) {
    unsigned char i=0;
//	modules initialization
    initIO();
    initSonar();
    initAcc1();
    
// main loop
    while(1){
        i=0;
        setLeds(i);
        while(!getButton2()); // push button 2 to start recording 
        initDatabk();
        for(i = 0;i<DATALENGTH;i++){
           setLeds(i);
           DelayMs(10);
           databkvar[0]=getButton1();
           databkvar[1]=i;
           databkvar[2]=i*4;
           dataStore(databkvar);
        }
        while(!getButton2()); // push button 2 to start sending data 
        dataRecovery();
        DelayMs(1000); 

    }
}


